from django.db import models
from random import choices

from matplotlib import widgets


# Create your models here.
dept_choices=(
    ('hr','Human Resources'),
    ('android','Android Developer'),
    ('web developer','Web Developer'),
    ('web designer','Web Designer'),
    ('data analyst','Data Analyst'),
    ('digital marketing','Digital Marketing')
)
role_choices=(
    ('senior','Senior'),
    ('junior','junior'),
    ('fresher','Fresher'),
)

# Create your models here.
class Employee(models.Model):
    name=models.CharField(max_length=30)
    email=models.EmailField(max_length=30)
    address=models.TextField()
    salary=models.IntegerField()
    department=models.CharField(max_length=30,choices=dept_choices)
    role=models.CharField(max_length=30,choices=role_choices)
