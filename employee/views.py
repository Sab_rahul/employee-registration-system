from audioop import reverse
from django.shortcuts import render
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView,UpdateView,DeleteView,FormView
from django.contrib.auth.views import LoginView,LogoutView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login
from .models import Employee
from django.contrib.auth.models import User
from django import forms
from .forms import UserRegisterForm
# Create your views here.

class EmployeeLoginView(LoginView):
    template_name= 'employee/login.html'
    redirect_authenticated_user=True
    
    def get_redirect_url(self):
        return reverse_lazy('employeelist')
    
    def get_form(self):
        form = super().get_form()
        form.fields['username'].widget = forms.TextInput(attrs={'class':'form-control', 'placeholder':'Enter Your Username'})
        form.fields['password'].widget = forms.PasswordInput(attrs={'class':'form-control', 'placeholder':'Enter Your Password'})
        return form
           
class RegisterFormView(CreateView):
    model=User
    form_class=UserCreationForm
    # fields=['username','email','password','password']
    template_name= 'employee/register.html'
    success_url = reverse_lazy("employeelist")

    def get_form(self):
        form = super().get_form()
        form.fields['username'].widget = forms.TextInput(attrs={'class':'form-control', 'placeholder':'Enter Your Username'})
        form.fields['password1'].widget = forms.PasswordInput(attrs={'class':'form-control', 'placeholder':'Enter Your Password'})
        form.fields['password2'].widget = forms.PasswordInput(attrs={'class':'form-control', 'placeholder':'Confirm Your Password'})
        return form


class EmployeeListView(LoginRequiredMixin, ListView):
    model=Employee
    template_name='employee/employeelist.html'
    
class EmployeeCreateView(LoginRequiredMixin, CreateView):
    model=Employee
    template_name='employee/employeecreate.html'
    fields=['name','email','address','salary','department','role']
    success_url= reverse_lazy('employeelist')

    def get_form(self):
        form = super().get_form()
        form.fields['name'].widget = forms.TextInput(attrs={'class':'form-control', 'placeholder':'Enter Employee Name'})
        form.fields['email'].widget = forms.EmailInput(attrs={'class':'form-control', 'placeholder':'Enter Employee Email'})
        form.fields['address'].widget = forms.TextInput(attrs={'class':'form-control', 'placeholder':'Enter Employee Address'})
        form.fields['salary'].widget = forms.NumberInput(attrs={'class':'form-control', 'placeholder':'Enter Employee Salary'})
        # form.fields['department'].widget = forms.Select(attrs={'class':'dropdown-item', 'placeholder':'Select Employee Department'})
        # form.fields['role'].widget = forms.Select(attrs={'class':'dropdown-item', 'placeholder':'Select Employee Role'})
        return form

class EmployeeDetailView(LoginRequiredMixin, DetailView):
    model=Employee
    template_name= 'employee/employeedetail.html'

class EmployeeUpdateView(LoginRequiredMixin, UpdateView):
    model=Employee
    template_name= 'employee/employeeupdate.html'
    success_url= '/'
    fields=['name','email','address','salary','department','role']

    def get_form(self):
        form = super().get_form()
        form.fields['name'].widget = forms.TextInput(attrs={'class':'form-control'})
        form.fields['email'].widget = forms.EmailInput(attrs={'class':'form-control'})
        form.fields['address'].widget = forms.TextInput(attrs={'class':'form-control'})
        form.fields['salary'].widget = forms.NumberInput(attrs={'class':'form-control'}) 
        # form.fields['department'].widget = forms.Select(attrs={'class':'dropdown-item', 'placeholder':'Select Employee Department'})
        # form.fields['role'].widget = forms.Select(attrs={'class':'dropdown-item', 'placeholder':'Select Employee Role'})
        return form

class EmployeeDeleteView(LoginRequiredMixin, DeleteView):
    model=Employee
    success_url= '/'



