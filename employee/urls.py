from django.contrib import admin
from django.urls import path
from . import views
from django.contrib.auth.views import LogoutView

urlpatterns = [
    path('',views.EmployeeListView.as_view(),name='employeelist'),
    path('login',views.EmployeeLoginView.as_view(),name='login'),
    path('register',views.RegisterFormView.as_view(),name='register'),
    path('logout/',views.LogoutView.as_view(next_page='login'),name='logout'),
    path('employeecreate/',views.EmployeeCreateView.as_view(),name='employeecreate'),
    path('employeedetail/<int:pk>',views.EmployeeDetailView.as_view(),name='employeedetail'),
    path('employeeupdate/<int:pk>',views.EmployeeUpdateView.as_view(),name='employeeupdate'),
    path('employeedelete/<int:pk>',views.EmployeeDeleteView.as_view(),name='employeedelete'),

]